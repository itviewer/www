#

SUBDIRS = styles inc images
CONV 	= ./html.py

FTR_PAGES = features.html
START_PAGES = manual.html technical.html occtl.html
RECIPES_PAGES = recipes.html
INDEX_PAGES = changelog.html download.html index.html packages.html clients.html
TOPLEVEL_PAGES = mail.html

ALL_PAGES = $(FTR_PAGES) $(START_PAGES) $(INDEX_PAGES) $(TOPLEVEL_PAGES) $(RECIPES_PAGES)

all: manual.html occtl.html $(ALL_PAGES)

update:
	mkdir public
	rsync -av ./ --exclude ocserv-git/ --exclude 'ocserv.8*' --exclude html.py --exclude 'Makefi*' --exclude '*~' --exclude '.git*' --exclude '*.xml' public/

clean:
	rm -f $(ALL_PAGES) occtl.8.inc ocserv.8.inc *~ $(RECIPES_PAGES) recipes*.xml recipes*.html

#$(ALL_PAGES): menu1.xml ./inc/*.tmpl
#$(FTR_PAGES): menu2-features.xml
#$(START_PAGES): menu2-started.xml
#$(MAIN_PAGES): menu2.xml

.PHONY: version.inc occtl.8.inc ocserv.8.inc changelog.inc recipes-all

manual.html: ocserv.8.inc
occtl.html: occtl.8.inc
download.html: version.inc
changelog.html: changelog.inc

BRANCH=master

ocserv-git.stamp:
	rm -rf ocserv-git
	git clone --depth 1 https://gitlab.com/ocserv/ocserv.git -b $(BRANCH) ocserv-git
	touch $@

changelog.inc: ocserv-git.stamp
	cat ./ocserv-git/NEWS|sed 's|\#\([0-9]\+\)|https://gitlab.com/ocserv/ocserv/issues/\1[\#\1]|g'|asciidoc -s -b xhtml11 - > $@

version.inc: ocserv-git.stamp
	cat ./ocserv-git/NEWS|grep "released 20"|head -1|cut -d ' ' -f 3 > $@

ocserv.8.inc: ocserv-git.stamp ./ocserv-git/doc/ocserv.8.md version.inc
	sed -e 's/^/    /' ocserv-git/doc/sample.config >sample.config.tmp
	sed -e '/@CONFIGFILE@/{r sample.config.tmp' -e 'd}' <./ocserv-git/doc/ocserv.8.md >ocserv.tmp
	ronn --pipe --html ocserv.tmp > $@.tmp
	test -s $@.tmp && mv $@.tmp $@
	rm -f sample.config.tmp

occtl.8.inc: ocserv-git.stamp ./ocserv-git/doc/occtl.8.md version.inc
	ronn --pipe --html ./ocserv-git/doc/occtl.8.md > $@

recipes-git.stamp:
	rm -rf $@
	git clone --depth 2 https://gitlab.com/openconnect/recipes.git recipes-git
	touch $@

recipes-all: recipes-git.stamp recipes.html
	for i in `cd recipes-git && ls *.md|sed 's/.md//g'`;do \
		$(MAKE) recipes-$$i.html; done

recipes.xml: recipes-all recipes-README.xml
	sed -i 's/\.md/.html/g' recipes-README.xml
	sed -i 's/href="o/href="recipes-o/g' recipes-README.xml
	cp recipes-README.xml recipes.xml

recipes-git/%.md: recipes-git.stamp

recipes-%.xml: recipes-git/%.md
	pandoc $^ -o $@-tmp
	sed -i 's/README\.md/recipes.html/g' $@-tmp
	sed -i 's/\.md/.html/g' $@-tmp
	sed -i 's/href="o/href="recipes-o/g' $@-tmp
	cat recipes-header.tmpl $@-tmp recipes-footer.tmpl > $@
	rm -f $@-tmp

%.html: %.xml
	$(CONV) -d . $< > $@ || (rm $@; exit 1)
